"use strict"

let webpack = require("webpack");
let path = require("path");
let process = require("process");
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var HtmlWebpackPlugin = require("html-webpack-plugin");
var WriteFilePlugin = require("write-file-webpack-plugin");
console.log(process.argv);
let isHotMode = process.argv.find(function (a) { return a.indexOf("webpack-dev-server") >= 0; }) && process.argv.find(function (a) { return a.indexOf("--hot") >= 0; });

console.log(isHotMode);

let config = {
    entry: {
        app: [
            "./src/index.tsx"
        ],
        vendor: [
            "react",
            "react-dom"
        ]
    },
    output: {
        path: path.resolve("dist"),
        filename: "[name].js",
        sourceMapFilename: "[file].map"
    },

    devtool: "source-map",

    resolve: {
        // Add '.ts' and '.tsx' as resolvable extensions.
        extensions: ["", ".webpack.js", ".web.js", ".ts", ".tsx", ".js"]
    },

    module: {
        loaders: [
            // All files with a '.ts' or '.tsx' extension will be handled by 'ts-loader'.
            { test: /\.tsx?$/, loaders: ["ts-loader"] }
        ],

        preLoaders: [
            // All output '.js' files will have any sourcemaps re-processed by 'source-map-loader'.
            { test: /\.js$/, loaders: ["source-map-loader"] }
        ]
    },

    plugins: [
        new HtmlWebpackPlugin({
            template: "./src/index.html",
            minify: { removeComments: true }
        })
    ]
};

if (isHotMode) {
    config.entry.hotload = [
        "webpack-dev-server/client?http://localhost:8080" // WebpackDevServer host and port
        //,"webpack/hot/dev-server" // "only" prevents reload on syntax errors
    ];

    config.output.publicPath = "http://localhost:8080/";

    config.module.loaders.find(function (l) { return l.test.toString() === /\.tsx?$/.toString() }).loaders.unshift("react-hot");
}

config.module.loaders.push({ test: /\.css/, loaders: ["style", "css"] });
config.module.loaders.push({ test: /\.(png|woff|woff2|eot|ttf|svg|gif)(\?.+)?(#.+)?$/, loaders: ["url-loader?limit=100000"] });

config.devServer = {
    headers: { "Access-Control-Allow-Origin": "*" },
    publicPath: "http://localhost:8080/",
    https: false,
    outputPath: path.resolve("./dist")
};

config.plugins.push(new WriteFilePlugin({ test: /\.html/ }));

module.exports = config;

