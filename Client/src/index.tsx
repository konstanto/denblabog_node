console.log("I'm here");
import * as React from "react";
import * as ReactDOM from "react-dom";

import { HelloWorld } from "./components/Hello";

ReactDOM.render(
    <HelloWorld compiler="hej" framework="uhu"/>,
    document.getElementById("app")
);