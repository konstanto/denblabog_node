import * as React from "react";

interface HelloWordProps { 
    compiler: string; 
    framework: string; 
}

export class HelloWorld extends React.Component<HelloWordProps, void> {
    render() {
        return (<h1>Hello from {this.props.compiler} and {this.props.framework}!</h1>);
    }
}
